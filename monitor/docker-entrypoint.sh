#!/bin/sh

cd /change-monitor

echo "Waitng for RabbitMQ on $RMQ_HOST:$RMQ_PORT"
until curl http://$RMQ_HOST:$RMQ_PORT/  > /dev/null 2>&1
do
  echo "$(date) - RabbitMQ is not ready yet"
  sleep 1
done
echo "$(date) - RabbitMQ is ready"

echo "Waitng for PostgreSQL on $DB_HOST:$DB_PORT"
until wget $DB_HOST:$DB_PORT -t 1 2>&1 | grep " connected"
do
    echo "$(date) - PostgreSQL is not ready yet"
    sleep 1
done
echo "$(date) - PostgreSQL is ready"

echo "Waitng for Elasticsearch on $ES_HOST:$ES_PORT"
until curl http://$ES_HOST:$ES_PORT/  > /dev/null 2>&1
do
  echo "$(date) - Elasticsearch is not ready yet"
  sleep 1
done
echo "$(date) - Elasticsearch is ready"

java -jar ./change-monitor.jar
