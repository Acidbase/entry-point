#!/bin/sh

cd /frontend

echo "Waitng for PostgreSQL on $PG_HOST:$PG_PORT"
until wget $PG_HOST:$PG_PORT -t 1 2>&1 | grep " connected"
do
    echo "$(date) - PostgreSQL is not ready yet"
    sleep 1
done
echo "$(date) - PostgreSQL is ready"

echo "Waitng for Elasticsearch on $ES_HOST:$ES_PORT"
until curl http://$ES_HOST:$ES_PORT/  > /dev/null 2>&1
do
  echo "$(date) - Elasticsearch is not ready yet"
  sleep 1
done
echo "$(date) - Elasticsearch is ready"

java -jar ./frontend.jar