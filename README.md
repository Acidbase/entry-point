**Acidbase** is a compound project which tries to overcome the shortcomings of the
RDBMS and NoSQL databases by combining them. In Acidbase, the data is stored in
an RDBMS (PostgreSQL) and a NoSQL (Elasticsearch) redundantly, providing an
eventually consistent database. When you are trying to write to the database,
you will be dealing with PostgreSQL, giving you a full set of ACID features.
On the other hand, the search is handled by Elasticsearch with BASE features
which is great at indexing and scalability.

As mentioned above, Acidbase is a compound project and entry-point is an easy
way to set it up and give it a test drive. Here are the steps to run the
project:

Prerequisites: `docker` (1.10.3+) and `docker-compose` (1.7.0+)

1. Download and extract the Acidbase files.
2. `cd ./entry-point`
3. `docker-compose build`
4. `docker-compose up`
5. Navigate to http://localhost:18080

In order to stop the services, in a new terminal:
1. `cd ./entry-point`
2. `docker-compose stop`
